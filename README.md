Fmentor
=======

Program for level up PyFML mentors.

Pep0008
-------

Read it carefully https://www.python.org/dev/peps/pep-0008/

FAQ
---

Read them carefully 

https://docs.python.org/3/faq/general.html

https://docs.python.org/3/faq/programming.html

https://docs.python.org/3/faq/design.html

Jupyter
-------

Get familiar with using Jupyter for python3.
Able to copy result and publish on gist.github.com.

HackerRank
----------

Get at least 500 points on python section.

Git
---

Proficient using git:
- using branch
- merging
- understand git status output
- knowing git MR workflow

ProjectEuler
------------

Finish at least 25 problems, push code to github.
Code must pep8 compliant.

TeachingAssistant
-----------------

Be teaching assistant in at least 1 course of PyFML.